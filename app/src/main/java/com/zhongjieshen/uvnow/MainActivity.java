package com.zhongjieshen.uvnow;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

// Add import statements for the new library.
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.PhotoMetadata;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPhotoRequest;
import com.google.android.libraries.places.api.net.PlacesClient;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import com.zhongjieshen.uvnow.databinding.ToolbarBinding;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;
    protected PlacesClient placesClient;

    /**
     * Provides the entry point to the Fused Location Provider API.
     */
    private FusedLocationProviderClient mFusedLocationClient;

    /**
     * Declare UI components.
     */
    private ImageButton refreshButton;
    private ImageButton getLocationButton;
    private TextView toolBarText;
    private TextView mLatitudeText;
    private TextView mLongitudeText;
    private TextView uvValueText;
    private ProgressBar mProgressBar;
    private ImageView placeImage;
    private ImageButton searchButton;
    private Toolbar toolbar;
    private String longitude;
    private String latitude;

    String placesApiKey = "AIzaSyDjcmBu9ecBvuEefl9MXTpzopVxtqDzJL4";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ToolbarBinding binding = DataBindingUtil.setContentView(
                this, R.layout.toolbar);
        PermissionStatus permissionStatus = new PermissionStatus();
        permissionStatus.setLocation(false);
        binding.setPermissionStatus(permissionStatus);

        initView();

        setUpListeners();

        setUpClients();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!checkPermissions()) {
            getLocationButton.setEnabled(false);
            getLocationButton.setVisibility(View.GONE);
            requestPermissions();
        } else {
            getLocationButton.setEnabled(true);
            getLocationButton.setVisibility(View.VISIBLE);
        }

    }

    private void setUpClients() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        // Initialize Places.
        Places.initialize(getApplicationContext(), placesApiKey);

        // Create a new Places client instance.
        placesClient = Places.createClient(this);

    }


    private void setUpListeners() {
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchForLocation();
            }

        });
        getLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                placeImage.setVisibility(View.GONE);
                placeImage.setImageBitmap(null);
                getLastLocation();
                uvValueText.setText(null);
                mProgressBar.setProgress(0);
                getUVIndex();
            }
        });
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUVIndex();
            }
        });
    }

    /**
     * Initialize view components
     */
    private void initView() {
        toolbar = findViewById(R.id.tool_bar);
        setContentView(R.layout.activity_main);
        Window window = this.getWindow();
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        setSupportActionBar(toolbar);
        toolBarText = findViewById(R.id.actionbar_text);
        mLatitudeText = findViewById((R.id.lat_edittext));
        mLongitudeText = findViewById((R.id.lon_edittext));
        searchButton = findViewById(R.id.search_place);
        getLocationButton = findViewById(R.id.locationButton);
        refreshButton = findViewById(R.id.floatingRefreshButton);
        uvValueText = findViewById(R.id.uv_index_text);
        mProgressBar = findViewById(R.id.uv_progressbar);
        placeImage = findViewById(R.id.place_image);

    }

    /**
     * Provides a simple way of getting a device's location and is well suited for
     * applications that do not require a fine-grained location and that do not need location
     * updates. Gets the best and most recent location currently available, which may be null
     * in rare cases when a location is not available.
     * <p>
     * Note: this method should be called after location permission has been granted.
     */
    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            requestPermissions();
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLastLocation = task.getResult();
                            toolBarText.setText(getString(R.string.current_location));
                            setLatLon(String.valueOf(mLastLocation.getLatitude()), String.valueOf(mLastLocation.getLongitude()));
                        } else {
                            Log.w(TAG, "getLastLocation:exception", task.getException());
                            showSnackbar(getString(R.string.no_location_detected));
                        }
                    }
                });
    }

    /**
     * Shows a {@link Snackbar} using {@code text}.
     *
     * @param text The Snackbar text.
     */
    private void showSnackbar(final String text) {
        View container = findViewById(R.id.main_activity_container);
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");

            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            startLocationPermissionRequest();
                        }
                    });

        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            startLocationPermissionRequest();
        }
    }

    /**
     * Callback received when another activity send results to here.
     * <i>(Location auto complete for this case)</i>
     *
     * @param requestCode A request code to differentiate requests.
     * @param resultCode  A result code returned from the activity.
     * @param data        An Intent.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);

                LatLng latLng = place.getLatLng();
                String pName = String.valueOf(place.getName());
                showSnackbar(getString(R.string.use_cooordinates_of) + ": " + pName);
                toolBarText.setText(pName);
                setLatLon(String.valueOf(latLng.latitude), String.valueOf(latLng.longitude));
                placeImage.setVisibility(View.GONE);
                getUVIndex();
                getPhotos(place);
                placeImage.setVisibility(View.VISIBLE);
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
                showSnackbar(getString(R.string.error));
                Log.i(TAG, status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                showSnackbar("The user canceled the operation");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (validateLatLon())
            getUVIndex();
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
                getLocationButton.setEnabled(false);
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLocationButton.setEnabled(true);
                getLocationButton.setVisibility(View.VISIBLE);
                getLastLocation();
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
                getLocationButton.setEnabled(false);
            }
        }
    }

    /**
     * Use Google Places: Place Autocomplete. Find the place and its coordinates.
     */
    private void searchForLocation() {
        // Setup a variable to define what fields are needed from Places
        List<Place.Field> fields = Arrays.asList(
                Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.PHOTO_METADATAS
        );

        Intent intent =
                new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
                        .build(this);
        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
    }

    /**
     * Request photos and metadata for the specified place.
     */
    private void getPhotos(Place p) {

            // Get the photo metadata.
            PhotoMetadata photoMetadata = p.getPhotoMetadatas().get(0);

            // Get the attribution text.
            String attributions = photoMetadata.getAttributions();

            // Create a FetchPhotoRequest.
            FetchPhotoRequest photoRequest = FetchPhotoRequest.builder(photoMetadata)
                    .setMaxWidth(500) // Optional.
                    .setMaxHeight(300) // Optional.
                    .build();

            placesClient.fetchPhoto(photoRequest).addOnSuccessListener((fetchPhotoResponse) -> {
                Bitmap bitmap = fetchPhotoResponse.getBitmap();
                placeImage.setImageBitmap(bitmap);
            }).addOnFailureListener((exception) -> {
                if (exception instanceof ApiException) {
                    ApiException apiException = (ApiException) exception;
                    int statusCode = apiException.getStatusCode();
                    // Handle error with given status code.
                    Log.e(TAG, "Place not found: " + exception.getMessage());
                }
            });
    }

    private void getUVIndex() {
        new getUVAsyncTask().execute();
    }

    private void setUIWithColor(double uvIndex) {
        Log.i(TAG, String.valueOf(uvIndex));

        mProgressBar.setMax(11);
        if (uvIndex > mProgressBar.getMax())
            uvIndex = mProgressBar.getMax();
        mProgressBar.setProgress(((int) uvIndex));

        String color = "#000000";
        if (uvIndex < 3)
            color = "#4CAF50";
        else if (uvIndex < 6)
            color = "#FFEB3B";
        else if (uvIndex < 8)
            color = "#FF4500";
        else if (uvIndex < 11)
            color = "#F44336";
        else if (11 <= uvIndex)
            color = "#9C27B0";

        mProgressBar.setProgressTintList(ColorStateList.valueOf(Color.parseColor(color)));
        uvValueText.setTextColor(ColorStateList.valueOf(Color.parseColor(color)));
    }

    /**
     * Set latitude and longitude fields and update UI
     *
     * @param lat latitude
     * @param lon longitude
     */
    private void setLatLon(String lat, String lon) {
        latitude = lat;
        longitude = lon;
        mLatitudeText.setText(latitude);
        mLongitudeText.setText(longitude);
    }

    private boolean validateLatLon() {
        if (null != latitude && null != longitude && !latitude.isEmpty() && !longitude.isEmpty())
            return true;
        else
            return false;
    }

    private class getUVAsyncTask extends AsyncTask<Void, Boolean, String> {

        // Do the long-running work in here
        @Override
        protected String doInBackground(Void... Void) {
            try {
                publishProgress(true);
                uvRequest uvRequest = new uvRequest();
                uvRequest.setLatitude(latitude);
                uvRequest.setLongitude(longitude);
                String uvValue = uvRequest.getUVIndex();
                publishProgress(false);
                return uvValue;
            } catch (Exception e) {
                publishProgress(false);
                showSnackbar(getString(R.string.error));
                this.cancel(true);
                return null;
            }

        }

        // This is called when doInBackground() is finished
        protected void onPostExecute(String uvValue) {
            if (null != uvValue && !uvValue.isEmpty()) {
                uvValueText.setText(uvValue);
                setUIWithColor(Double.valueOf(uvValue));
                mProgressBar.setVisibility(View.VISIBLE);
            }
        }

        // This is called each time you call publishProgress()
        protected void onProgressUpdate(Boolean... run) {
            if (run[0]) {
                mProgressBar.setIndeterminate(true);
                mProgressBar.setVisibility(View.VISIBLE);
            } else {
                mProgressBar.setIndeterminate(false);
                mProgressBar.setVisibility(View.GONE);
            }

        }
    }


}