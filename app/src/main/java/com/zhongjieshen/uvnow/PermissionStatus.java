package com.zhongjieshen.uvnow;

/**
 * Created by shenz on 2017-12-18.
 */


public class PermissionStatus {
    private boolean location = false;

    public PermissionStatus() {

    }

    public Boolean getLocation() {
        return location;
    }

    public void setLocation(Boolean location) {
        this.location = location;
    }

}
