package com.zhongjieshen.uvnow;


import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by shenz on 2018-01-17.
 */

public class uvRequest {
    private String latitude;
    private String longitude;
    private String api_address = "https://api.openweathermap.org/data/2.5/uvi?";
    private String appid = "5190cb6bcd84a13de4da109d4c9d5b41";

    public String getUVIndex() {
        try {
            if (validateLatLon())
                return fireUvRequest();
            else
                return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setApi_address(String api_address) {
        this.api_address = api_address;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    private boolean validateLatLon() {
        if (!latitude.isEmpty() && !longitude.isEmpty())
            return true;
        else
            return false;
    }

    private String fireUvRequest() throws Exception {
        String urlToRead = api_address + "appid=" + appid + "&" + "lat=" + latitude + "&" + "lon=" + longitude;

        String jsonString = getJsonString(urlToRead);

        JSONObject uvJson = new JSONObject(jsonString);
        return uvJson.getString("value");
    }

    private String getJsonString(String urlToRead) throws Exception {
        StringBuilder result = new StringBuilder();
        URL url = new URL(urlToRead);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");

        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        rd.close();
        return result.toString();
    }

}
