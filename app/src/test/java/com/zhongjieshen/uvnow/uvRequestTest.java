package com.zhongjieshen.uvnow;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Created by shenz on 2018-01-20.
 */
public class uvRequestTest {
    @Test(timeout = 1000)
    public void getUVIndex() throws Exception {
        uvRequest request = new uvRequest();
        request.setLatitude("49.26");
        request.setLongitude("-123.24");
        String uvValue = request.getUVIndex();
        assertNotNull(uvValue);
    }
}